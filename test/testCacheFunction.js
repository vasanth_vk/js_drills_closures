const cacheFunction = require('../cacheFunction');
let square = cacheFunction((e) => {
    let res = []
    for (let i in e) {

        res.push(Math.sqrt(e[i]))
    }
    return res.length > 1 ? res : res[0] || null
}
)

console.log(square(81));
console.log(square(4, 9, 16, 25));
console.log(square());
console.log(square(144));
console.log(square(144));

let sum = cacheFunction((e) => {
    let res = 0;
    for (let el in e) {
        res += e[el];
    }
    return res;
})

console.log(sum(1, 2, 3));
console.log(sum(1));
console.log(sum());


