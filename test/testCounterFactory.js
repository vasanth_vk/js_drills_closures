const counterFactory = require("../counterFactory");

let myCounter = counterFactory();
console.log("=====counter1======");
console.log(myCounter.increment());
console.log(myCounter.increment());
console.log(myCounter.increment());
console.log(myCounter.increment());

console.log(myCounter);
console.log(myCounter.decrement());
console.log(myCounter.decrement());
console.log("=====counter2======");
let myCounter2 = counterFactory();
console.log(myCounter2.increment());
console.log(myCounter.increment());
