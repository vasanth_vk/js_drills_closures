// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
function limitFunctionCallCount(cb, n) {
    function invoke() {
        n -= 1;
        if (n >= 0) {
            return cb();
        } else {
            return null;
        }
    }
    return invoke;
}


module.exports = limitFunctionCallCount;